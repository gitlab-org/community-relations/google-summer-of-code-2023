# GitLab + Google Summer of Code, 2023
This is the main place for all information related to GitLab’s participation in Google Summer of Code, 2023 as a mentoring organization.

## What is Google Summer of Code?

[Google Summer of Code](https://summerofcode.withgoogle.com/) is a 16 years old program, run every summer, with the intention of bringing more students into open source. 

Open source projects apply as mentor organizations and if they are accepted, students send proposals to them to work on a few months' long project. Projects can be planned out by the organizations in advance or can be proposed by students.

Google pays the students, not the organizations they work with. Beginning in 2023, Google is opening the program up to all newcomers of open source that are 18 years and older.

You can read more about the format of the program and its goals [here](https://google.github.io/gsocguides/mentor/).

Some more links with interesting information:

- [Expanding Google Summer of Code in 2022](https://opensource.googleblog.com/2021/11/expanding-google-summer-of-code-in-2022.html)

- Organizations that participated in [2020](https://summerofcode.withgoogle.com/archive/2020/organizations/), [2019](https://summerofcode.withgoogle.com/archive/2019/organizations/), [2018](https://summerofcode.withgoogle.com/archive/2018/organizations/)

- [Guidelines for mentoring organizations/what is needed of us?](https://google.github.io/gsocguides/mentor/)

- [GSoC 2023 rules](https://summerofcode.withgoogle.com/rules/)

## Why should GitLab participate?
GSoC could be a great addition to [our internship pilot program](https://about.gitlab.com/handbook/engineering/internships/) and on-going DIB efforts. GSoC gives us the opportunity to mentor students from around the world.

## What is needed from GitLab?
We will need a few teams to host interns, with planned out projects.

Each GSoC project requires 1 (or maybe 2) mentor per student and a person from the company who can be Google's contact person for the duration of the program.

In addition to GSoC's format, our own experience with [the internship pilot program](https://about.gitlab.com/handbook/engineering/internships/) can help us plan out our participation.

## What is the timeline for GSoC 2023?
[Full timeline](https://summerofcode.withgoogle.com/programs/2023)

|Important events | Deadline|
| ----- | ----- |
| Organization Applications Open | January 23, 2023|
| Organization Application Deadline | February 7, 2023 |
| Organizations Announced | February 22, 2023 |
| Potential GSoC contributors discuss application ideas with mentoring organizations | February 22 - March 20, 2023 |
| GSoC contributor application period | March 20 - April 4, 2023 |
| Accepted GSoC Contributor projects announced | May 4, 2023 |
| Students work on their Google Summer of Code project | May 4, 2023 - NOvember 17, 2023|

# How to propose a project?
To propose a project, open an issue in this project with a [pre-built template (project proposal)](https://gitlab.com/gitlab-org/community-relations/google-summer-of-code-2023/-/issues/new?issuable_template=project-proposal).

## What does a project proposal require?
When proposing a project, mention the GitLab project and the repository link along with a description of the project goals.

GitLab members who wish to be mentors, should make sure they have their managers' approval for the project and time commitment.

## Who can propose a project and who can be a mentor?

GitLab members and members from the wider community can both propose projects, however, only GitLab members can be mentors.

# What is expected of a mentor?
Please read [the mentor guide](mentor-guide.md).

# Information for applying students

Students should have knowledge of git, ruby, and markdown (GitLab CI/CD is a plus) for most projects since the project work heavily depends on them. 

We invite students to look into [our open proposals](https://gitlab.com/gitlab-org/community-relations/google-summer-of-code-2023/-/issues), ask mentors questions to understand the projects better and if interested [apply for the project when the application period opens](https://summerofcode.withgoogle.com/programs/2023/organizations/gitlab).

Mentors would like to know why the project interests the student, whether they have the pre-requisite skills, and most importantly, how they plan to implement it.

We encourage students to set up Gitlab for local development and play around with the code and tests to get more comfortable with the project. They can find information on how to start contributing to GitLab here https://about.gitlab.com/community/contribute .

# Contact
Feel free to open an issue in this repo to start a conversation with us.

@nick_vh is the DRIs on this initiative.

